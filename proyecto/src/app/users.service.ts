import { HttpClient  } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { observableToBeFn } from 'rxjs/internal/testing/TestScheduler';

@Injectable({
  providedIn:'root'
})

export class UsersService {
constructor(private http: HttpClient){
   
   }

  public getUsers(): Observable<any[]> {
    return this.http.get<any[]>('http://localhost:8080/product');
  }

  public saveUser(users: any[]){
    localStorage.setItem('users', JSON.stringify(users));
  }

  public createUser(user: any): Observable<any> {  
  return this.http.post('http://localhost:8080/product', user);
  }

  public updateUser(id: string, data: any): Observable<any> {
    return this.http.put(`http://localhost:8080/product/${id}`, data);
  }

  public deleteUser(id: string): Observable<any> {
    return this.http.delete(`http://localhost:8080/product/${id}`);
  }


  public removeUser(idUser: string): Observable<void> {
    return this.http.delete<void>('http://localhost:8080/product'.concat(idUser));

  }

}




