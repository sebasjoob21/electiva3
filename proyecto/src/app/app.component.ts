import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
import {user} from '../app/user' 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'userManagement';
  user = { username: '', name: '' , age: '' , Num_phone: '' , city: '' }
  users :any[] =[];

  public constructor
  (private usersService: UsersService){}
  
   public ngOnInit(){
    this.usersService.getUsers().subscribe((result) =>{
      this.users = result;
    });
  }

  public getUsers(): void {
    this.usersService.getUsers().subscribe((data) => {
        this.users = data;
      });
  }
    
  public createUser(user:any){
 this.usersService.createUser(user).subscribe((data) =>{
  this.user = data;
  this.getUsers();
 });
}

public updateUser(id: string, data: any){
  this.usersService.updateUser(id, data).subscribe((response) => {
      this.users = response;
      this.getUsers();
    });
}

public deleteUser(id: string){
  this.usersService.deleteUser(id).subscribe((response) => {
      this.user = response;
      this.getUsers();
    });
}

public eliminarUsuario(index : number){
  this.users[index];
  this.usersService.removeUser(this.users[index].idUsers).subscribe(() => {
    console.log("Registro elminado");
  });

}
public guardarUsuarios(index :number){
  this.usersService.saveUser(this.users) ;
}
}
