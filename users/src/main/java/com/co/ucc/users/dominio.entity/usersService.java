package com.co.ucc.users.dominio;

import com.co.ucc.users.dominio.entuty.User;

import java.util.List;

@Service
public class usersService {

    @Autowired
    private UsersRepository usersRepository;

    public List <User> getUsers(){
        return usersRepository.getUsers();
    }

    public User getUserById(Long id){

        List<User> allUsers = usersRepository.getUser();
        Optional<User> foundUser = allUsers.stream().filter(user -> Objects.equals(user.id, id)).findFirst();
        return foundUser.get();
    }

    public User createUser(User user){
        return usersRepository.createUser(user);

    }

    public User updateUser(User user ) {
        return usersRepository.updateUser(user);
    }

    public void User deleteUser(Long id ){
     usersRepository.deleteUser(id);
    }

}
    
