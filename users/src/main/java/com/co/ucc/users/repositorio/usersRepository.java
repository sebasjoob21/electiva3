package com.co.ucc.users.repositorio;

@Repository
public class usersRepository {

    private static List<User> users = new ArrayList<>();



    public class usersService {

        public List <User> getUsers(){
            return users;
        }  

        public User createUser(User user) {
            users.add(user);
            return user;
        }

        public User updateUser(User user ) {
            for (int i = 0; i< users.size(); i++){
                if(users.get(i).id.equals(user.id)){
                    users.get(i).name = user.name;
                }
            }
            return user;
        }

        public void User deleteUser(Long id ){

            for (int i = 0; i < users.size(); index++){
                if (users.get(index).id.equals(id)){
                    users.remove(index);
                    break;
                }
            }
        }
    } 
}
