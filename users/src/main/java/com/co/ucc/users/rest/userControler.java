package com.co.ucc.users.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class userControler {

    @Autowired
    private UsersService usersService;
    
    @GetMapping("/users")
    public List<User> getUsers(){
        return usersService.getUsers();
    }

    @GetMapping("/users/{id}")
    public User getUser(@PathVariable Long id) {
        return usersService.getUsers();
    }

    @PostMapping("/users")
    public User createUser(@RequestBody User user) {
        return usersService.;
    }

    @putMapping("/users/{id}")
    public User updateUser(User user) {
        return usersRepository.updateUser(user);
    }

    @DeleteMapping("/users/{id}")
    public void User deleteUser(@PathVariable Long id) {
     usersRepository.deleteUser(id);
    }
}
