package co.com.ucc.products.rest;

import co.com.ucc.products.dominio.entity.User;
import co.com.ucc.products.dominio.UserService;
import co.com.ucc.products.repositorio.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/users")
    public List<UserModel> getUser(){
        return userService.getUser();
    }

    @GetMapping("/users/{id}")
    public UserModel getUser(@PathVariable Long idUser) {
        return userService.getUserById(idUser);
    }

    @PostMapping("/users")
    public UserModel createUser(@RequestBody UserModel user) {
        return userService.createUser(user);
    }

    @PutMapping("/users/{id}")
    public UserModel updateUser(@RequestBody UserModel user) {
        return userService.updateUser(user);
    }

    @DeleteMapping("/users/{id}")
    public void deleteUser(@PathVariable Long idUser) {
        userService.deleteUser(idUser);
    }
}

