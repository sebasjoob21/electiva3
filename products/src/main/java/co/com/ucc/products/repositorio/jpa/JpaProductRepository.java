package co.com.ucc.products.repositorio.jpa;
import co.com.ucc.products.repositorio.model.ProductModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaProductRepository extends JpaRepository<ProductModel, Long> {

}
