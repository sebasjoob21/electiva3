package co.com.ucc.products.dominio;

import co.com.ucc.products.repositorio.UserRepository;
import co.com.ucc.products.repositorio.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<UserModel> getUser(){
        return userRepository.getUser();
    }

    public UserModel getUserById(Long idUser){
        return UserRepository.findById(idUser);
    }

    public UserModel createUser(UserModel user){
        return userRepository.createUser(user);

    }

    public UserModel updateUser(UserModel user ) {
        return userRepository.updateUser(user);
    }

    public void deleteUser( Long idUser){
        userRepository.deleteUser(idUser);
    }

}

