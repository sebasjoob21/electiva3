package co.com.ucc.products.rest;

import co.com.ucc.products.dominio.entity.Person;
import co.com.ucc.products.dominio.PersonService;
import co.com.ucc.products.repositorio.model.PersonModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PersonController {

    @Autowired
    private PersonService personService;

    @GetMapping("/person")
    public List<PersonModel> getPerson(){
        return personService.getPerson();
    }

    @GetMapping("/persons/{name}")
    public Person getPerson(@PathVariable String name) {
        return personService.getPersonByname(name);
    }

    @PostMapping("/persons")
    public PersonModel createPerson(@RequestBody PersonModel person) {
        return personService.createPerson(person);}

    @PutMapping("/persons/{name}")
    public PersonModel updatePerson(PersonModel person) {
        return personService.updatePerson(person);
    }

    @DeleteMapping("/persons/{name}")
    public void  deletePerson(@PathVariable String name) {
        personService.deletePerson(name);
    }
}

