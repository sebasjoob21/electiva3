package co.com.ucc.products.repositorio.jpa;
import co.com.ucc.products.repositorio.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaUserRepository extends JpaRepository<UserModel, Long> {
}
