package co.com.ucc.products.repositorio.jpa;
import co.com.ucc.products.repositorio.model.PersonModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface JpaPersonRepository extends JpaRepository<PersonModel, String> {

}
