package co.com.ucc.products.rest;


import co.com.ucc.products.dominio.ProductService;
import co.com.ucc.products.repositorio.model.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ProductControler {

    @Autowired
    private ProductService productService;

    @GetMapping("/product")
    public List<ProductModel> getProduct(){
        return productService.getProduct();
    }

    @GetMapping("/products/{id}")
    public ProductModel getProduct(@PathVariable Long id) {
        return productService.getProductById(id);
    }

    @PostMapping("/products")
    public ProductModel createProduct(@RequestBody ProductModel product) {
        return productService.createProduct(product);}

    @PutMapping("/products/{id}")
    public ProductModel updateProduct(ProductModel product) {
        return productService.updateProduct(product);
    }

    @DeleteMapping("/products/{id}")
    public void  deleteProduct(@PathVariable Long id) {
        productService.deleteProduct(id);
    }
}
