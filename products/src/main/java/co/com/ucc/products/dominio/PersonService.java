package co.com.ucc.products.dominio;

import co.com.ucc.products.dominio.entity.Person;
import co.com.ucc.products.repositorio.PersonRepository;
import co.com.ucc.products.repositorio.model.PersonModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<PersonModel> getPerson(){
        return personRepository.getPerson();
    }

    public Person getPersonByname(String name){

        /*List<Person> allPersons = personRepository.getPerson();
        Optional<Person> foundPerson = allPersons.stream().filter(person -> Objects.equals(person.name, name)).findFirst();
        */
        return null;
    }

    public PersonModel createPerson(PersonModel person){
        return personRepository.createPerson(person);

    }

    public PersonModel updatePerson(PersonModel person ) {
        return personRepository.updatePerson(person);
    }

    public PersonModel deletePerson(@PathVariable("name")String name){
       return personRepository.deletePerson(name);
    }
}
