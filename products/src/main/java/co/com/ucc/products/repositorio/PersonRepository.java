package co.com.ucc.products.repositorio;

import co.com.ucc.products.dominio.entity.Person;
import co.com.ucc.products.repositorio.jpa.JpaPersonRepository;
import co.com.ucc.products.repositorio.model.PersonModel;
import co.com.ucc.products.repositorio.model.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PersonRepository {
    @Autowired
    private JpaPersonRepository jpaPersonRepository;
    private static final List<Person> persons = new ArrayList<>();
    private int index;

    public List<PersonModel> getPerson() {
        return jpaPersonRepository.findAll();
    }

    public PersonModel createPerson(PersonModel person) {
        return jpaPersonRepository.save(person);
    }

    public PersonModel updatePerson(PersonModel person) {
       return jpaPersonRepository.save(person);
    }

    public PersonModel deletePerson(String name) {
       jpaPersonRepository.deleteById(name);
        return null;
    }
}
