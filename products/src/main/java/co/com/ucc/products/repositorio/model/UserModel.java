package co.com.ucc.products.repositorio.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="User")
public class UserModel {

    public UserModel(){

    }

    public UserModel(Long idUser){
        this.idUser = idUser;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
            private  Long idUser;
            private  String username;
            private  String pasword;

            public Long getId() {
            return idUser;
            }

            public void setId(Long id) {
               this.idUser = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }
}
