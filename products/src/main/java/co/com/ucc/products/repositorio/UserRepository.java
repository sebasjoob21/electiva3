package co.com.ucc.products.repositorio;

import co.com.ucc.products.dominio.entity.User;
import co.com.ucc.products.repositorio.jpa.JpaUserRepository;
import co.com.ucc.products.repositorio.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
@Repository
public class UserRepository {
    @Autowired
    private static JpaUserRepository jpaUserRepository;

    public List<UserModel> getUser() {
        return jpaUserRepository.findAll();
    }

    public UserModel createUser(UserModel user) {
        return jpaUserRepository.save(user);
    }

    public UserModel updateUser(UserModel user) {
        return jpaUserRepository.save(user);
    }

    public void deleteUser(long idUser) {
        jpaUserRepository.delete(new UserModel(idUser));
    }
    public static UserModel findById(Long idUser){
        return jpaUserRepository.findById(idUser).get();
    }
}
