package co.com.ucc.products.dominio;

import co.com.ucc.products.repositorio.ProductRepository;
import co.com.ucc.products.repositorio.model.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductModel> getProduct(){
        return productRepository.getProduct();
    }

    public ProductModel getProductById(Long id) {
        return productRepository.findById(id);
    }

    public ProductModel createProduct(ProductModel product){
        return productRepository.createProduct(product);

    }

    public ProductModel updateProduct(ProductModel product ) {
        return productRepository.updateProduct(product);
    }

    public ProductModel  deleteProduct(@PathVariable("id") Long id){
        return productRepository.deleteProduct(id);
    }
}

