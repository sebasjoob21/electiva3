package co.com.ucc.products.repositorio;

import co.com.ucc.products.dominio.entity.Product;
import co.com.ucc.products.repositorio.jpa.JpaProductRepository;
import co.com.ucc.products.repositorio.model.ProductModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class ProductRepository {

    @Autowired
    private JpaProductRepository jpaProductRepository;

    private static final List<Product> products = new ArrayList<>();
    private int index;

    public List<ProductModel> getProduct() {
        return jpaProductRepository.findAll();
    }

    public ProductModel createProduct(ProductModel product) {
        return jpaProductRepository.save(product);
    }

    public ProductModel updateProduct(ProductModel product) {
        return jpaProductRepository.save(product);
    }

    public ProductModel deleteProduct(Long id) {
        jpaProductRepository.deleteById(id);
        return null;
    }

    public ProductModel findById(Long id) {
        return jpaProductRepository.findById(id).get();
    }
}


